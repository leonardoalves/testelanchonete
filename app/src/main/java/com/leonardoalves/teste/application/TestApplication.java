package com.leonardoalves.teste.application;

import android.support.multidex.MultiDexApplication;

import io.realm.Realm;

/**
 * Created by leonardo on 24/08/17.
 */

public class TestApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}

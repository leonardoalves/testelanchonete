package com.leonardoalves.teste.presenter;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwich;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwichListener;
import com.leonardoalves.data.repository.Sandwiches.SandwichInfoListener;
import com.leonardoalves.data.repository.Sandwiches.SandwichInfoServer;
import com.leonardoalves.domain.IngredientsCustomization;
import com.leonardoalves.domain.IngredientsCustomizationListener;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by leonardo on 23/08/17.
 */

public class SandwichInfoPresenter {

    final SandwichInfoPresenterListener listener;

    public SandwichInfoPresenter(SandwichInfoPresenterListener listener) {
        this.listener = listener;
    }

    public void getSandwichInfo(int id){
        SandwichInfoServer sandwichInfoServer = new SandwichInfoServer();
        sandwichInfoServer.getSandwich(id, new SandwichInfoListener() {
            @Override
            public void onResult(Sandwich sandwich) {
                listener.getSandwichInfo(sandwich);
                IngredientsFromSandwich ingredientsFromSandwich = new IngredientsFromSandwich();
                ingredientsFromSandwich.getIngredientFromSandwich(sandwich, new IngredientsFromSandwichListener() {
                    @Override
                    public void onResult(ArrayList<Ingredient> ingredients) {
                        listener.addIngredient(ingredients);
                    }

                    @Override
                    public void onError(Throwable error) {
                        listener.error(error.toString());
                    }
                });
            }

            @Override
            public void onError(Throwable t) {
                listener.error(t.toString());
            }
        });
    }

    public void calculateTotalPrice(ArrayList<Ingredient> ingredients) {
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (Ingredient ingredient : ingredients) {
            totalPrice = totalPrice.add(new BigDecimal(ingredient.getPrice()));
        }
        listener.updateTotalPrice(totalPrice.toString());
    }
}

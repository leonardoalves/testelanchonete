package com.leonardoalves.teste.presenter;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;

import java.util.ArrayList;

/**
 * Created by leonardo on 23/08/17.
 */

public interface SandwichInfoPresenterListener {

    void getSandwichInfo(Sandwich sandwich);
    void addIngredient(ArrayList<Ingredient> ingredient);
    void error(String error);
    void updateTotalPrice(String totalPrice);
}

package com.leonardoalves.teste.presenter;

import com.leonardoalves.data.entity.viewData.SandwichViewData;

import java.util.ArrayList;

/**
 * Created by leona on 23/08/2017.
 */

public interface GetSandwichesPresenterListener {
    void getListOfSandwiches(ArrayList<SandwichViewData> list);
    void addSandwichToList(SandwichViewData sandwichViewData);

    void showError(String error);
}

package com.leonardoalves.teste.presenter;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.repository.Sandwiches.SandwichesListListener;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.data.repository.Sandwiches.SandwichesListServer;
import com.leonardoalves.domain.IngredientsCustomization;
import com.leonardoalves.domain.IngredientsCustomizationListener;
import com.leonardoalves.domain.SandwichDataListener;
import com.leonardoalves.domain.SandwichMount;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by leona on 23/08/2017.
 */

public class SandwichListPresenter {
    SandwichMount sandwichMount = new SandwichMount();

    public void getSandwiches(final GetSandwichesPresenterListener listener){
        SandwichesListServer sandwichesListServer = new SandwichesListServer();
        sandwichesListServer.getSandwiches(new SandwichesListListener() {
            @Override
            public void onResult(ArrayList<Sandwich> sandwiches) {
                for (final Sandwich sandwich : sandwiches) {
                    sandwichMount.completeSandwichData(sandwich, new SandwichDataListener() {
                        @Override
                        public void sandwichData(ArrayList<Ingredient> ingredients, BigDecimal totalPrice) {
                            String ingredientLabel = "";
                            for (int i = 0; i < ingredients.size(); i++) {
                                Ingredient ingredient = ingredients.get(i);
                                ingredientLabel += ingredient.getName();
                                if (i != ingredients.size() - 1){
                                    ingredientLabel += ", ";
                                }
                            }
                            SandwichViewData sandwichViewData = new SandwichViewData(
                                    sandwich.getId(),
                                    sandwich.getName(),
                                    "R$ " + totalPrice.setScale(2).toString(),
                                    sandwich.getImage(),
                                    ingredientLabel
                            );
                            listener.addSandwichToList(sandwichViewData);
                        }

                        @Override
                        public void error(Throwable error) {
                            listener.showError(error.toString());
                        }
                    });
                }
            }

            @Override
            public void onError(Throwable error) {
                System.out.println("erro"+error.getMessage());
            }
        });
    }
}

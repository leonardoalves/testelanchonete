package com.leonardoalves.teste.view.activity;

import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.domain.IngredientLoad.IngredientLoad;
import com.leonardoalves.teste.R;
import com.leonardoalves.teste.presenter.GetSandwichesPresenterListener;
import com.leonardoalves.teste.presenter.SandwichListPresenter;
import com.leonardoalves.teste.view.adapter.SandwichsListAdapter;
import com.leonardoalves.teste.view.menu.BottomMenuListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottomNavigationView)
    public BottomNavigationView bottomNavigationView;
    @BindView(R.id.sandwichs_list)
    public RecyclerView mRecyclerView;

    private SandwichsListAdapter mAdapter;
    private SandwichListPresenter presenter;
    ArrayList<SandwichViewData> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new SandwichListPresenter();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

        myDataset = new ArrayList<>();
        mAdapter = new SandwichsListAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomMenuListener(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.getSandwiches(new GetSandwichesPresenterListener() {
            @Override
            public void getListOfSandwiches(ArrayList<SandwichViewData> list) {
                int size = list.size();
                myDataset.clear();
                myDataset.addAll(list);
                mAdapter.notifyItemInserted(size);
            }

            @Override
            public void addSandwichToList(SandwichViewData sandwichViewData) {
                myDataset.add(sandwichViewData);
                mAdapter.notifyItemInserted(myDataset.size());
            }

            @Override
            public void showError(String error) {
                Snackbar snackbar = Snackbar
                        .make(mRecyclerView, error, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
        IngredientLoad.loadAditionalData();
    }
}

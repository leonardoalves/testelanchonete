package com.leonardoalves.teste.view.menu;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.leonardoalves.domain.shoppingcart.ShoppingCart;
import com.leonardoalves.teste.R;
import com.leonardoalves.teste.view.activity.MainActivity;

/**
 * Created by leonardo on 25/08/17.
 */

public class BottomMenuListener implements BottomNavigationView.OnNavigationItemSelectedListener {
    Context context;

    public BottomMenuListener(Context context) {
        this.context = context;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.menu_products:
                 intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                return true;
            case R.id.menu_sales:
                return true;
            case R.id.menu_cart:
                intent = new Intent(context, ShoppingCart.class);
                context.startActivity(intent);
                return true;
        }
        return false;
    }
}

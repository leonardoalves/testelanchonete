package com.leonardoalves.teste.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.teste.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leonardo on 23/08/17.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolder> {
    ArrayList<Ingredient> dataset = new ArrayList<>();

    public IngredientAdapter(ArrayList<Ingredient> dataset) {
        this.dataset = dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ingredient_cell, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Ingredient ingredient = dataset.get(position);
        holder.name.setText(ingredient.getName());
        holder.price.setText(String.valueOf(ingredient.getPrice()));
        Ion.with(holder.photo)
                .load(ingredient.getImage());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        @BindView(R.id.ingredient_name)
        public TextView name;
        @BindView(R.id.ingredient_price)
        public TextView price;
        @BindView(R.id.ingredient_photo)
        public ImageView photo;
        protected View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}

package com.leonardoalves.teste.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.teste.R;
import com.leonardoalves.teste.presenter.shoppingCart.ShoppingCartPresenter;
import com.leonardoalves.teste.presenter.shoppingCart.ShoppingCartPresenterListener;
import com.leonardoalves.teste.view.adapter.SandwichsListAdapter;
import com.leonardoalves.teste.view.menu.BottomMenuListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leonardo on 25/08/17.
 */

public class ShoppingCartActivity extends AppCompatActivity implements ShoppingCartPresenterListener{

    @BindView(R.id.bottomNavigationView)
    public BottomNavigationView bottomNavigationView;
    @BindView(R.id.cart_list)
    public RecyclerView recyclerView;

    private ShoppingCartPresenter presenter;
    ArrayList<SandwichViewData> dataSet;
    private SandwichsListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        ButterKnife.bind(this);

        presenter = new ShoppingCartPresenter();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);

        dataSet = new ArrayList<>();
        adapter = new SandwichsListAdapter(dataSet);
        recyclerView.setAdapter(adapter);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomMenuListener(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.showShoppingCart(this);
    }
}

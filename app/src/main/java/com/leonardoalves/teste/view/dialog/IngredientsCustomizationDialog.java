package com.leonardoalves.teste.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.domain.IngredientsCustomization;
import com.leonardoalves.teste.R;

import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public class IngredientsCustomizationDialog extends DialogFragment {

    private IngredientsCustomizationDialogListener listener;
    private ArrayList<Integer> mSelectedItems;
    private ArrayList<Ingredient> ingredientsOptions;
    private CharSequence[] ingredientsNames;

    public interface IngredientsCustomizationDialogListener {
        public void ingredientsCustomizationDialogPositiveClick(ArrayList<Ingredient> selectedIngredients);
        public void ingredientsCustomizationDialogNegativeClick();
    }

    //For android before 5.0
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (IngredientsCustomizationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    //For 5.0 and above
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            try {
                listener = (IngredientsCustomizationDialogListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement NoticeDialogListener");
            }
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.
        mSelectedItems = new ArrayList();  // Where we track the selected items
        ingredientsNames = loadIngredientsName();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.pick_ingredients)
                .setMultiChoiceItems(ingredientsNames, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    mSelectedItems.add(which);
                                } else if (mSelectedItems.contains(which)) {
                                    mSelectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.add_ingredients, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        ArrayList<Ingredient> selected = new ArrayList<>();
                        for (Integer mSelectedItem : mSelectedItems) {
                            Ingredient ingredient = new Ingredient();
                            ingredient.setId(ingredientsOptions.get(mSelectedItem).getId());
                            ingredient.setImage(ingredientsOptions.get(mSelectedItem).getImage());
                            ingredient.setName(ingredientsOptions.get(mSelectedItem).getName());
                            ingredient.setPrice(ingredientsOptions.get(mSelectedItem).getPrice());
                            selected.add(ingredient);
                        }
                        listener.ingredientsCustomizationDialogPositiveClick(selected);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.ingredientsCustomizationDialogNegativeClick();
                    }
                });

        return builder.create();
    }

    private CharSequence[] loadIngredientsName() {
        IngredientsCustomization ingredientsCustomization = new IngredientsCustomization();
        ingredientsOptions = ingredientsCustomization.getIngredientsOptions();
        CharSequence[] options = new CharSequence[ingredientsOptions.size()];
        for (int i = 0; i < ingredientsOptions.size(); i++) {
            options[i] = ingredientsOptions.get(i).getName();
        }
        return options;
    }
}

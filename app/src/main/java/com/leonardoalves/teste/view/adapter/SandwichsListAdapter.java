package com.leonardoalves.teste.view.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.teste.R;
import com.leonardoalves.teste.view.activity.SandwichInfoActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leona on 23/08/2017.
 */

public class SandwichsListAdapter extends RecyclerView.Adapter<SandwichsListAdapter.ViewHolder> {
    ArrayList<SandwichViewData> dataset;

    public static final String EXTRA_MESSAGE = "com.leonardoalves.teste.view.adapter.SANDWICH";

    public SandwichsListAdapter(ArrayList<SandwichViewData> dataset) {
        this.dataset = dataset;
    }

    @Override
    public SandwichsListAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sandwich_cell, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(SandwichsListAdapter.ViewHolder holder, final int position) {
        SandwichViewData sandwich = dataset.get(position);
        holder.name.setText(sandwich.getName());
        holder.price.setText(String.valueOf(sandwich.getPrice()));
        holder.ingredients.setText(sandwich.getIngredients());
        Ion.with(holder.photo)
                .load(sandwich.getPhotoURL());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SandwichInfoActivity.class);
                intent.putExtra(EXTRA_MESSAGE, dataset.get(position).getId());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        public TextView name;
        @BindView(R.id.ingredients)
        public TextView ingredients;
        @BindView(R.id.price)
        public TextView price;
        @BindView(R.id.photo)
        public ImageView photo;
        protected View view;
        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}

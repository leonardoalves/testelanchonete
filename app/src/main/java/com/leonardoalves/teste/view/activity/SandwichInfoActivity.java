package com.leonardoalves.teste.view.activity;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.leonardoalves.data.entity.data.CartItem;
import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.ShoppingCart.AddItemCartServerListener;
import com.leonardoalves.data.repository.ShoppingCart.ShoppingCartServer;
import com.leonardoalves.domain.IngredientsCustomization;
import com.leonardoalves.domain.shoppingcart.AddItemShoppingCartListener;
import com.leonardoalves.domain.shoppingcart.ShoppingCart;
import com.leonardoalves.teste.R;
import com.leonardoalves.teste.presenter.SandwichInfoPresenter;
import com.leonardoalves.teste.presenter.SandwichInfoPresenterListener;
import com.leonardoalves.teste.presenter.SandwichListPresenter;
import com.leonardoalves.teste.view.adapter.IngredientAdapter;
import com.leonardoalves.teste.view.adapter.SandwichsListAdapter;
import com.leonardoalves.teste.view.dialog.IngredientsCustomizationDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leonardo on 23/08/17.
 */

public class SandwichInfoActivity extends AppCompatActivity implements IngredientsCustomizationDialog.IngredientsCustomizationDialogListener {

    private int sandwichId;
    private SandwichInfoPresenter sandwichInfoPresenter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<Ingredient> myDataset;
    private IngredientAdapter mAdapter;
    @BindView(R.id.buy)
    public Button buy;
    @BindView(R.id.personalize)
    public Button personalize;
    @BindView(R.id.sandwich_price)
    public TextView sandwichPrice;

    private SandwichInfoPresenterListener sandwichInfoPresenterListener;
    private IngredientsCustomization ingredientsCustomization;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sandwich_detail);

        Intent intent = getIntent();
        sandwichId = intent.getIntExtra(SandwichsListAdapter.EXTRA_MESSAGE, 0);
        ingredientsCustomization = new IngredientsCustomization();

        mRecyclerView = (RecyclerView) findViewById(R.id.ingredients_list);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        myDataset = new ArrayList<>();
        mAdapter = new IngredientAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);

        ButterKnife.bind(this);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShoppingCart.addItem(sandwichId, ingredientsCustomization.getExtras(), new AddItemShoppingCartListener() {
                    @Override
                    public void onSuccess() {
                        Snackbar.make(mRecyclerView, "Item adicionado ao Carrinho.", Snackbar.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError() {
                        Snackbar.make(mRecyclerView, "Não foi possivel acionar o item", Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        });
        personalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngredientsCustomizationDialog ingredientsCustomizationDialog = new IngredientsCustomizationDialog();
                ingredientsCustomizationDialog.show(getFragmentManager(), "Personalize");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sandwichInfoPresenterListener = new SandwichInfoPresenterListener() {
            @Override
            public void getSandwichInfo(Sandwich sandwich) {
                TextView textView = (TextView) findViewById(R.id.sandwich_name);
                textView.setText(sandwich.getName());
                ImageView image = (ImageView) findViewById(R.id.sandwich_photo);
                Ion.with(image)
                        .load(sandwich.getImage());
            }

            @Override
            public void addIngredient(ArrayList<Ingredient> ingredient) {
                int size = ingredient.size();
                myDataset.addAll(ingredient);
                mAdapter.notifyItemInserted(myDataset.size() - size);
                sandwichInfoPresenter.calculateTotalPrice(myDataset);
            }

            @Override
            public void error(String error) {
                System.out.println(error);
            }

            @Override
            public void updateTotalPrice(String price) {
                sandwichPrice.setText(price);
                System.out.println("ingredients");
            }
        };
        sandwichInfoPresenter = new SandwichInfoPresenter(sandwichInfoPresenterListener);
        sandwichInfoPresenter.getSandwichInfo(sandwichId);
    }

    @Override
    public void ingredientsCustomizationDialogPositiveClick(ArrayList<Ingredient> selectedIngredients) {
        int size = selectedIngredients.size();
        myDataset.addAll(selectedIngredients);
        mAdapter.notifyItemInserted(myDataset.size() - size);
        ingredientsCustomization.addExtra(selectedIngredients);
        sandwichInfoPresenter.calculateTotalPrice(myDataset);
    }

    @Override
    public void ingredientsCustomizationDialogNegativeClick() {

    }
}

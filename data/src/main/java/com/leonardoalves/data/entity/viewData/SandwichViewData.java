package com.leonardoalves.data.entity.viewData;

import java.math.BigDecimal;

/**
 * Created by leona on 23/08/2017.
 */

public class SandwichViewData {
    int id;
    String name;
    String price;
    String photoURL;
    String ingredients;

    public SandwichViewData(int id, String name, String price, String photoURL, String ingredients) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.photoURL = photoURL;
        this.ingredients = ingredients;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }
}

package com.leonardoalves.data.entity.data;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_sandwich")
    @Expose
    private String idSandwich;
    @SerializedName("extras")
    @Expose
    private List<Integer> extras = null;
    @SerializedName("date")
    @Expose
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdSandwich() {
        return idSandwich;
    }

    public void setIdSandwich(String idSandwich) {
        this.idSandwich = idSandwich;
    }

    public List<Integer> getExtras() {
        return extras;
    }

    public void setExtras(List<Integer> extras) {
        this.extras = extras;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
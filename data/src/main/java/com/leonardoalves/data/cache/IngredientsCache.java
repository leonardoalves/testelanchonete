package com.leonardoalves.data.cache;

import com.leonardoalves.data.entity.data.Ingredient;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by leonardo on 24/08/17.
 */

public class IngredientsCache {
    private final Realm realm;

    public IngredientsCache() {
        realm = Realm.getDefaultInstance();
    }

    public void storeIngredients(ArrayList<Ingredient> ingredients){
        for (final Ingredient ingredient : ingredients) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(ingredient);
                }
            });
        }
    }

    public ArrayList<Ingredient> getIngredients(){
        ArrayList<Ingredient> result = new ArrayList<>();
        RealmResults<Ingredient> all = realm.where(Ingredient.class).findAll();
        for (Ingredient ingredient : all) {
            result.add(ingredient);
        }
        return result;
    }
}

package com.leonardoalves.data.repository.ShoppingCart;

import android.util.Log;

import com.leonardoalves.data.entity.data.CartItem;
import com.leonardoalves.data.repository.ApiClient;
import com.leonardoalves.data.repository.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leonardo on 24/08/17.
 */

public class ShoppingCartServer {
    private static final String TAG = "PUT_ITEM_CART";

    public void addItem(int id, Integer[] extras, final AddItemCartServerListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        String bodyField = "[";
        for (int i = 0; i < extras.length; i++) {
            bodyField += extras[i];
            if(i != extras.length -1){
                bodyField += ",";
            }
        }
        bodyField +="]";
        Call<CartItem> call = apiService.addShoppingCart(id, bodyField);
        call.enqueue(new Callback<CartItem>() {
            @Override
            public void onResponse(Call<CartItem>call, Response<CartItem> response) {
                CartItem item = response.body();
                listener.onResult(item);
            }

            @Override
            public void onFailure(Call<CartItem>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }

    public void getShoppingCart(final GetShoppingCartListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<CartItem>> call = apiService.getShoppingCart();
        call.enqueue(new Callback<ArrayList<CartItem>>() {
            @Override
            public void onResponse(Call<ArrayList<CartItem>>call, Response<ArrayList<CartItem>> response) {
                ArrayList<CartItem> item = response.body();
                listener.onResult(item);
            }

            @Override
            public void onFailure(Call<ArrayList<CartItem>>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }
}

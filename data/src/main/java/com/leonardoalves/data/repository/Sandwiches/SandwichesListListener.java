package com.leonardoalves.data.repository.Sandwiches;

import com.leonardoalves.data.entity.data.Sandwich;

import java.util.ArrayList;

/**
 * Created by leona on 23/08/2017.
 */

public interface SandwichesListListener {
    void onResult(ArrayList<Sandwich> sandwiches);
    void onError(Throwable error);
}

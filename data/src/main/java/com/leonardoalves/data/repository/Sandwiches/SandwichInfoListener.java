package com.leonardoalves.data.repository.Sandwiches;

import com.leonardoalves.data.entity.data.Sandwich;

/**
 * Created by leonardo on 23/08/17.
 */

public interface SandwichInfoListener {

    void onResult(Sandwich sandwich);

    void onError(Throwable t);
}

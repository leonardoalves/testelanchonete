package com.leonardoalves.data.repository.Ingredients;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;

import java.util.ArrayList;

/**
 * Created by leonardo on 23/08/17.
 */

public interface IngredientsFromSandwichListener {
    void onResult(ArrayList<Ingredient> ingredients);
    void onError(Throwable error);
}

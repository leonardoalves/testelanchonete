package com.leonardoalves.data.repository;

import com.leonardoalves.data.entity.data.CartItem;
import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by leona on 23/08/2017.
 */

public interface ApiInterface {
    @GET("lanche")
    Call<ArrayList<Sandwich>> getSandwiches();
    @GET("ingrediente/de/{id}")
    Call<ArrayList<Ingredient>> getIngredientFromSandwich(@Path("id") int id);
    @GET("ingrediente/")
    Call<ArrayList<Ingredient>> getIngredients();
    @GET("lanche/{id_lanche}")
    Call<Sandwich> getSandwichData(@Path("id_lanche") int id);
    @GET("pedido")
    Call<ArrayList<CartItem>> getShoppingCart();
    @FormUrlEncoded
    @PUT("pedido/{id_lanche}")
    Call<CartItem> addShoppingCart (@Path("id_lanche") int id, @Field("extras") String extras);

}

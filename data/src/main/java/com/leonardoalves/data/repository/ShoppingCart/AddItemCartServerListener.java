package com.leonardoalves.data.repository.ShoppingCart;

import com.leonardoalves.data.entity.data.CartItem;

/**
 * Created by leonardo on 24/08/17.
 */

public interface AddItemCartServerListener {

    void onResult(CartItem item);

    void onError(Throwable t);
}

package com.leonardoalves.data.repository.Ingredients;

import android.util.Log;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.ApiClient;
import com.leonardoalves.data.repository.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leonardo on 23/08/17.
 */

public class IngredientsFromSandwich {
    private static final String TAG = "GET_SANDWICH_INGREDIENT";

    public void getIngredientFromSandwich(Sandwich sandwich, final IngredientsFromSandwichListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Ingredient>> call = apiService.getIngredientFromSandwich(sandwich.getId());
        call.enqueue(new Callback<ArrayList<Ingredient>>() {
            @Override
            public void onResponse(Call<ArrayList<Ingredient>>call, Response<ArrayList<Ingredient>> response) {
                ArrayList<Ingredient> sandwiches = response.body();
                listener.onResult(sandwiches);
            }

            @Override
            public void onFailure(Call<ArrayList<Ingredient>>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }
}

package com.leonardoalves.data.repository.Sandwiches;

import android.util.Log;

import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.ApiClient;
import com.leonardoalves.data.repository.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leonardo on 23/08/17.
 */

public class SandwichInfoServer {
    private static final String TAG = "GET_SANDWICH";

    public void getSandwich(int id, final SandwichInfoListener listener){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Sandwich> call = apiService.getSandwichData(id);
        call.enqueue(new Callback<Sandwich>() {
            @Override
            public void onResponse(Call<Sandwich>call, Response<Sandwich> response) {
                Sandwich sandwich = response.body();
                listener.onResult(sandwich);
            }

            @Override
            public void onFailure(Call<Sandwich>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }
}

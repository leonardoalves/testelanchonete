package com.leonardoalves.data.repository.ShoppingCart;

import com.leonardoalves.data.entity.data.CartItem;

import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public interface GetShoppingCartListener {
    void onResult(ArrayList<CartItem> itens);

    void onError(Throwable t);

}

package com.leonardoalves.data.repository.Sandwiches;

import android.util.Log;

import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.ApiClient;
import com.leonardoalves.data.repository.ApiInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leona on 23/08/2017.
 */

public class SandwichesListServer {

    private static final String TAG = "GET_SANDWICHES";

    public void getSandwiches(final SandwichesListListener listener){

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ArrayList<Sandwich>> call = apiService.getSandwiches();
        call.enqueue(new Callback<ArrayList<Sandwich>>() {
            @Override
            public void onResponse(Call<ArrayList<Sandwich>>call, Response<ArrayList<Sandwich>> response) {
                ArrayList<Sandwich> sandwiches = response.body();
                listener.onResult(sandwiches);
            }

            @Override
            public void onFailure(Call<ArrayList<Sandwich>>call, Throwable t) {
                Log.e(TAG, t.toString());
                listener.onError(t);
            }
        });
    }

}

package com.leonardoalves.domain.IngredientLoad;

import com.leonardoalves.data.cache.IngredientsCache;
import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwichListener;
import com.leonardoalves.data.repository.Ingredients.IngredientsRepository;
import com.leonardoalves.domain.IngredientsCustomization;
import com.leonardoalves.domain.IngredientsCustomizationListener;

import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public class IngredientLoad {
    public static void loadAditionalData(){
        IngredientsRepository ingredientsRepository = new IngredientsRepository();
        ingredientsRepository.getIngredients(new IngredientsFromSandwichListener() {
            @Override
            public void onResult(ArrayList<Ingredient> ingredients) {
                IngredientsCache ingredientsCache = new IngredientsCache();
                ingredientsCache.storeIngredients(ingredients);
            }

            @Override
            public void onError(Throwable error) {
                System.out.println("Ingredientes não carregados");
            }
        });
    }
}

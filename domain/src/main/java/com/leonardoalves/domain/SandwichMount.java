package com.leonardoalves.domain;

import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.entity.viewData.SandwichViewData;
import com.leonardoalves.data.repository.Ingredients.IngredientsRepository;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwich;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwichListener;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by leonardo on 23/08/17.
 */

public class SandwichMount {

    public void completeSandwichData(Sandwich sandwich, final SandwichDataListener sandwichDataListener){
        IngredientsFromSandwich ingredientsFromSandwich = new IngredientsFromSandwich();
        ingredientsFromSandwich.getIngredientFromSandwich(sandwich, new IngredientsFromSandwichListener() {
            @Override
            public void onResult(ArrayList<Ingredient> ingredients) {
                BigDecimal totalPrice = BigDecimal.ZERO;
                for (Ingredient ingredient : ingredients) {
                    totalPrice = totalPrice.add(new BigDecimal(ingredient.getPrice()));
                }
                sandwichDataListener.sandwichData(ingredients, totalPrice);
            }

            @Override
            public void onError(Throwable error) {
                sandwichDataListener.error(error);
            }
        });
    }
}

package com.leonardoalves.domain;

import com.leonardoalves.data.entity.data.Ingredient;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by leonardo on 23/08/17.
 */

public interface SandwichDataListener {
    void sandwichData(ArrayList<Ingredient> ingredients, BigDecimal totalPrice);
    void error(Throwable error);
}

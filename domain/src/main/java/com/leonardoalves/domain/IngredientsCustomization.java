package com.leonardoalves.domain;

import com.leonardoalves.data.cache.IngredientsCache;
import com.leonardoalves.data.entity.data.Ingredient;
import com.leonardoalves.data.repository.Ingredients.IngredientsFromSandwichListener;
import com.leonardoalves.data.repository.Ingredients.IngredientsRepository;

import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public class IngredientsCustomization {

    ArrayList<Integer> extras = new ArrayList<>();

    public ArrayList<Ingredient> getIngredientsOptions(){
        IngredientsCache ingredientsCache = new IngredientsCache();
        return ingredientsCache.getIngredients();
    }

    public void addExtra(ArrayList<Ingredient> ingredients){
        for (Ingredient ingredient : ingredients) {
            extras.add(ingredient.getId());
        }
    }

    public Integer[] getExtras(){
        Integer[] extras = new Integer[this.extras.size()];
        extras = this.extras.toArray(extras);
        return extras;
    }

}

package com.leonardoalves.domain;

import com.leonardoalves.data.entity.data.Ingredient;

import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public interface IngredientsCustomizationListener {
    void ingredientsLists(ArrayList<Ingredient> ingredients);
}

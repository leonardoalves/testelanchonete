package com.leonardoalves.domain.shoppingcart;

import com.leonardoalves.data.entity.data.CartItem;
import com.leonardoalves.data.entity.data.Sandwich;
import com.leonardoalves.data.repository.Sandwiches.SandwichInfoListener;
import com.leonardoalves.data.repository.Sandwiches.SandwichInfoServer;
import com.leonardoalves.data.repository.ShoppingCart.AddItemCartServerListener;
import com.leonardoalves.data.repository.ShoppingCart.GetShoppingCartListener;
import com.leonardoalves.data.repository.ShoppingCart.ShoppingCartServer;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by leonardo on 24/08/17.
 */

public class ShoppingCart {
    public static void addItem(int sandwichId, Integer[] extras, final AddItemShoppingCartListener listener){
        ShoppingCartServer shoppingCartServer = new ShoppingCartServer();
        shoppingCartServer.addItem(sandwichId, extras, new AddItemCartServerListener() {
            @Override
            public void onResult(CartItem item) {
                listener.onSuccess();
            }

            @Override
            public void onError(Throwable t) {
                listener.onError();
            }
        });
    }

    public static void shoppingCartItens(final ShoppingCartListListener listListener){
        ShoppingCartServer shoppingCartServer = new ShoppingCartServer();
        shoppingCartServer.getShoppingCart(new GetShoppingCartListener() {
            @Override
            public void onResult(ArrayList<CartItem> itens) {
                listListener.onResult(itens);
            }

            @Override
            public void onError(Throwable t) {
                listListener.onError(t.toString());
            }
        });
    }

    public static void calculateItemPrice(CartItem item, final UpdatePriceListener listener){
        SandwichInfoServer sandwichInfoServer = new SandwichInfoServer();
        sandwichInfoServer.getSandwich(Integer.valueOf(item.getIdSandwich()), new SandwichInfoListener() {
            @Override
            public void onResult(Sandwich sandwich) {
                BigDecimal price = BigDecimal.ZERO;
                for (Integer integer : sandwich.getIngredients()) {
                    price = price.add()
                }
            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }
}

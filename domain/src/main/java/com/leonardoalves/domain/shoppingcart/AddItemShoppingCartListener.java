package com.leonardoalves.domain.shoppingcart;

/**
 * Created by leonardo on 24/08/17.
 */

public interface AddItemShoppingCartListener {
    void onSuccess();
    void onError();
}

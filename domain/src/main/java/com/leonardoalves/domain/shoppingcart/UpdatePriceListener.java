package com.leonardoalves.domain.shoppingcart;

import java.math.BigDecimal;

/**
 * Created by leonardo on 25/08/17.
 */

public interface UpdatePriceListener {
    void updatePrice(BigDecimal price);
}

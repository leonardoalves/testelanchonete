package com.leonardoalves.domain.shoppingcart;

import com.leonardoalves.data.entity.data.CartItem;

import java.util.ArrayList;

/**
 * Created by leonardo on 25/08/17.
 */

interface ShoppingCartListListener {

    void onResult(ArrayList<CartItem> itens);
    void onError(String error);
}
